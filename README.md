#onboarding
# Contributing

This is a shared Obsidian vault powered by a Gitlab repository.

## Installing the repository

1. Install Git: https://git-scm.com/ and follow the instructions
2. Install Obsidian: https://obsidian.md/
3. Clone this repository:
	1. Open a terminal in your host, and go to the parent folder where you want the folder to be created.
	2. Clone the repository: `git clone https://gitlab.com/leikt.solreihin/test-obsidian-git.git`
4. Launch Obsidian
5. Open the folder as vault
6. Turn on Community Plugins